import React from "react";

const Person = ({data,pr}) => {
  return (
    <div className="container d-flex">
      <div style={{ width: "300px", height: "250px" }} className="personCover">
        <img className="img-fluid" src="./glassesImage/model.jpg" alt="..." />
        <div>
          <img className="personGlass" src={pr.url} alt="" />
        </div>
        <div className="bg-secondary w-100 personDetail">
          <div className="d-flex justify-content-around">
            <span className="text-white font-weight-bold">{pr.name}</span>
            <span className="text-white text-white">{pr.price}$</span>
          </div>
          <p className="text-white">{pr.desc}</p>
        </div>
      </div>
      <div className="ml-auto" style={{ width: "300px", height: "250px" }}>
        <img className="img-fluid" src="./glassesImage/model.jpg" alt="..." />
      </div>
    </div>
  );
};

export default Person;

import React from 'react'
import GlassItem from './GlassItem';

const Glass = ({data,handlePrList}) => {
  return (
    <div className='row glassCover bg-secondary'>
      {data.map((item)=> {
        return (
          <GlassItem pr={item} key={item.id} handlePrList={handlePrList}/>
        );
      })}
      
    </div>
  )
}

export default Glass
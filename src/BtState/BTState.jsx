import React, { useState } from 'react'
import './style.css'
import data from './data.json'
import Person from './Person'
import Glass from './Glass'
const BTState = () => {
  const [prList, setPrList] = useState(data[0]);
  const handlePrList = (pr) => {
    setPrList(pr);
  };

  return (
    <div className='coverPage'>
        <div className='text-center bg-dark text-white' style={{opacity:0.9,height:'80px'}}>
            <h2 className='title'>TRY GLASSES APP ONLINE</h2>
        </div>
        <Person pr={prList}/>
        <div className="container">
          <Glass data={data} handlePrList={handlePrList}/>
        </div>

    </div>
  )
}

export default BTState
import React from "react";

const GlassItem = ({ pr,handlePrList }) => {
  return (
    <div className="col-2 border ml-2 my-2 glassItem">
      <img
        className="img-fluid"
        onClick={() => {
          handlePrList(pr);
        }}
        src={pr.url}
        alt="..."
      />
    </div>
  );
};

export default GlassItem;
